# bitview-back
Contains Django application
Every 1 minute fetch data from https://api.bittrex.com/v3/markets/BTC-USDT/summary
Parses this data received to Django Models VirtualCurrency
Exposes an endpoint to server a list of Virtual Currencies paginated.

# install back
brew install pyenv-virtualenv
brew install zlib
cd bitviewer-back
pyenv virtualenv 3.7.0 bitviewer
pyenv activate bitviewer
pip install -r requirements.txt

# run server (http://localhost:8000)
python manage.py runserver
Now back is running on port 8000

# run script to sync data (from another terminal)
python manage.py sync_markets