from django.db import models

class VirtualCurrency(models.Model):
    symbol = models.CharField(max_length=50)
    high = models.DecimalField(null=True, decimal_places=8, max_digits=20)
    low = models.DecimalField(null=True, decimal_places=8, max_digits=20)
    date_updated = models.DateTimeField(editable=False, auto_now_add=True)
    url_fetch = models.CharField(max_length=200)
    raw_response = models.TextField(max_length=1000)
    created_at = models.DateTimeField(editable=False, auto_now_add=True)
    updated_at = models.DateTimeField(editable=False, auto_now=True)

    def __str__(self):
        return f'{self.pk}'
    
    class Meta:
        verbose_name_plural = "Virtual currencies"
