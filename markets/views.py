from rest_framework import viewsets, permissions
from .models import VirtualCurrency
from .serializers import VirtualCurrencySerializer


class VirtualCurrencyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = VirtualCurrency.objects.all().order_by('-created_at')
    serializer_class = VirtualCurrencySerializer
    permission_classes = [permissions.AllowAny,]
    
