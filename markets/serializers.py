from rest_framework import serializers
from .models import VirtualCurrency

class VirtualCurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = VirtualCurrency
        fields = ['symbol', 'high', 'low', 'date_updated']

     