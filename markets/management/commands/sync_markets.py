import time, asyncio, json, requests
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime, timedelta
from markets.models import VirtualCurrency

class Command(BaseCommand):
    help = 'Fetch data from external url and register it on VirtualCurrency '
    TIME_SLEEP = 60 # seconds
    TIME_RECONNECT = 10 # seconds
    URL_FETCH_DATA = 'https://api.bittrex.com/v3/markets/BTC-USDT/summary'

    def handle(self, *args, **options):
        '''
            Infinite loop executed every TIME_SLEEP seconds.
            If fails try to execute it on TIME_RECONNECT seconds.
        '''
        try:
            while True:
                try:
                    asyncio.get_event_loop().run_until_complete(self.sync_markets())
                    time.sleep(self.TIME_SLEEP)
                except Exception as error:
                    print('[error] => ', error)
                    time.sleep(self.TIME_RECONNECT)
        except Exception as error:
            raise CommandError('[error]', error)

    async def sync_markets(self):
        ''' 
            Get to URL_FETCH_DATA
            Map data received and register it on VirtualCurrency Model
            Infinite loop executed every TIME_SLEEP seconds
        '''
        try:
            print('[sync_markets] => start', datetime.now())
            response = requests.get(str(self.URL_FETCH_DATA), json={})
            body_response = json.loads(response.text)
            if response.status_code != requests.codes.ok:
                raise Exception(str(response))
            virtual_currency = VirtualCurrency()
            virtual_currency.url_fetch = self.URL_FETCH_DATA
            virtual_currency.symbol = body_response['symbol']
            virtual_currency.high = body_response['high']
            virtual_currency.low = body_response['low']
            virtual_currency.date_updated = datetime.strptime(body_response['updatedAt'], '%Y-%m-%dT%H:%M:%S.%fZ')
            virtual_currency.raw_response = str(body_response)
            virtual_currency.save()
            print('[sync_markets] => end', virtual_currency.date_updated)
        except Exception as error:
            print('[sync_markets] => error', error)
