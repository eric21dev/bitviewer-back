from django.contrib import admin
from markets.models import VirtualCurrency


@admin.register(VirtualCurrency)
class VirtualCurrencyAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'high', 'low', 'date_updated')
    ordering = ['-date_updated',]
    search_fields = ['user__username', 'id']
    list_filter = ('symbol', 'date_updated')
    list_per_page = 20
    readonly_fields = ('url_fetch','raw_response', 'symbol', 'high', 'low', 'date_updated', 'created_at')
