from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter
from markets.views import VirtualCurrencyViewSet

router = DefaultRouter()
router.register(r'api/v1/virtualCurrencies', VirtualCurrencyViewSet)

urlpatterns = [
    path('', admin.site.urls),
]
urlpatterns += router.urls
